import React from 'react';
import 'normalize.css'  
import './styles.css'

class InputForm extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            inputValue: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)

    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.onSubmit(this.state.inputValue)
        this.setState({inputValue: ''})
    }

    handleInputChange(inputValue) {
        this.setState({inputValue})
    }

    render() {
        return (
            <form
                className='FormContainer'
                onSubmit={this.handleSubmit}
            >
                <input 
                    className='FormInput'
                    type='text' 
                    placeholder='Add new task'
                    value={this.state.inputValue}
                    onChange={e => this.handleInputChange(e.target.value)}
                />
                <input
                    type='submit'
                    value='+'
                />
            </form>
        )
    }
}

export default InputForm;
