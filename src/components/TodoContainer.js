import React from 'react';
import './styles.css'

class TodoContainer extends React.Component {



 	render() {
    	return (
	    	<div className='TodoContainer'>
		        <h2 
		        	className='Header'
		        >
		        	Things to do:
		        </h2>

		        {this.props.items.map((item, index) =>
		        	<div className='Item' key={index}>
		        		<span className='itemText'>{item}</span>
		        		<button className='DeleteButton'
		        				onClick={() => this.props.onDelete(index)}
		        		>
		        			x
		        		</button>
		        	</div>

		        )}

	        </div>
    	)
  	}
}

export default TodoContainer;