import React from 'react';
import './components/styles.css'
import InputForm from './components/InputForm.js'
import TodoContainer from './components/TodoContainer.js'

class App extends React.Component {

    constructor() {
        super()

        this.state = {
            items: []
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
    }

    handleSubmit(item) {
        if (item.length) {
            const itemArray = this.state.items
            itemArray.push(item)
            this.setState({items: itemArray})
        }
    }

    handleDelete(index) {
        const itemsArray = this.state.items
        itemsArray.splice(index, 1)
        this.setState({items: itemsArray})
    }

    render() {
        return (
            <div className='ContentContainer'>
                <h1 className = 'Header'>Todo List</h1>
                <InputForm
                    onSubmit={this.handleSubmit}
                />
                <TodoContainer
                    items={this.state.items}
                    onDelete={this.handleDelete}
                />
            </div>
        )
    }
}

export default App;
